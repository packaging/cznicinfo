class CZNICInfoException(Exception):
    msg_fmt = "An unknown error occurred"
    exit_code = 1

    def __init__(self, msg=None, exit_code=None, **kwargs):
        self.kwargs = kwargs
        if not msg:
            try:
                msg = self.msg_fmt % kwargs
            except Exception:
                # kwargs doesn't match those in message.
                # Returning this is still better than nothing.
                msg = self.msg_fmt
        if exit_code is not None:
            self.exit_code = exit_code
        super(CZNICInfoException, self).__init__(msg)


class InvalidInfoStructure(CZNICInfoException):
    msg_fmt = "Invalid info structure"
    exit_code = 8


class MissingRequiredInfoItem(InvalidInfoStructure):
    msg_fmt = "Missing required info item: %(item)s"


class DuplicateInfoItem(InvalidInfoStructure):
    msg_fmt = "Duplicate info item: %(item)s"


class DuplicateInfoProjects(InvalidInfoStructure):
    msg_fmt = "Duplicate info projects: %(item)s"


class InsufficientInfo(CZNICInfoException):
    msg_fmt = "Insufficient information available"
    exit_code = 10


class InsufficientProjectInfo(InsufficientInfo):
    msg_fmt = ("Project '%(proj)s' is missing information required "
               "to fulfill your request: %(item)s")


class InvalidProjectFilter(CZNICInfoException):
    msg_fmt = "Invalid filter: %(why)s"
    exit_code = 12


class InvalidType(CZNICInfoException):
    msg_fmt = "Invalid type: $(var) must be %(desired)s but is %(type)s"
    exit_code = 14


class InvalidChoice(CZNICInfoException):
    msg_fmt = "Invalid choice: %(var)s must be one of: %(opts)s (is: %(val)s)"
    exit_code = 16


class InvalidFormat(CZNICInfoException):
    msg_fmt = "Invalid format: %(fmt)s"
    exit_code = 18


class OutputExists(CZNICInfoException):
    msg_fmt = "Output exists: %(out)s"
    exit_code = 30


class HTTPRequestFailed(CZNICInfoException):
    msg_fmt = "HTTP request failed with code %(code)s: %(request)s"
    exit_code = 44


class CommandNotFound(CZNICInfoException):
    msg_fmt = "Command not found: %(cmd)s"
    exit_code = 60


class CommandFailed(CZNICInfoException):
    msg_fmt = "Command failed: %(cmd)s"
    exit_code = 62
