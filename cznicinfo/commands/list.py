"""
List available projects

Usage: cznicinfo list [-h]
"""

from docopt import docopt

from cznicinfo import infocore
from cznicinfo import infoprint


def run_command(*args, **kwargs):
    docopt(__doc__)

    info = infocore.get_info()
    infoprint.pretty_list_projects(info['projects'])

    return 0
