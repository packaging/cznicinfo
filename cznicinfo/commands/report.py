"""
Generate report of various project(s) versions

Usage: cznicinfo report [-h] [-c] [-f] [-o <dir>] [<project>...]

Arguments:
    <project>     project(s) to generate report for (default: ALL)

Options:
    -o <dir>, --outdir <dir>  generate report into this directory
    -c, --cached              don't check versions, use cached data instead
    -f, --force               use force - delete outdir if it exists
    -h, --help                show command help

"""

from datetime import datetime
from docopt import docopt
import os
from pathlib import Path
import shutil

from cznicinfo import cache
from cznicinfo import exception
from cznicinfo import infocore
from cznicinfo import infoquery
from cznicinfo.log import getLogger
from cznicinfo import nv
from cznicinfo import template


log = getLogger(__name__)


def run_command(*args, **kwargs):
    cargs = docopt(__doc__)

    info = infocore.get_info()

    if cargs['<project>']:
        projs, not_found = infoquery.get_projects_by_name(
            info, cargs['<project>'])
        if not_found:
            nf_str = ", ".join(not_found)
            log.warn("following projects were not found: %s", nf_str)
    else:
        # all projects by default
        projs = info.get('projects')

    if not cargs['--cached']:
        log.info("checking latest versions...")
        for proj in projs:
            nv.save_config(proj)
            nv.run_check(proj)

    if cargs['--outdir']:
        outdir = Path(cargs['--outdir'])
        if outdir.exists():
            if cargs['--force']:
                log.verbose("removing existing report dir with --force: %s",
                            outdir)
                shutil.rmtree(outdir)
            else:
                msg = "selected --outdir exists (override with -f/--force)"
                raise exception.OutputExists(msg=msg)
    else:
        outdir = cache.report_cache_path
        if outdir.exists():
            # cached report dir can be overwritten without force
            shutil.rmtree(outdir)

    log.info("generating version report...")
    os.makedirs(outdir)
    index = gen_versions_report(projs, outdir)

    print("report index: %s" % index)

    return 0


def gen_versions_report(projects, out_dir):
    out_dir = Path(out_dir)
    for proj in projects:
        vers_by_distro = nv.versions_by_distro(proj)
        proj['version_tables'] = nv.versions_by_distro2tables(vers_by_distro)
        proj['version_counts'] = nv.count_versions_tags(vers_by_distro)

    vars = {
        'projects': projects,
        'date': datetime.now(),
    }
    index = template.render_template('report/index.html', out_dir, vars)
    template.copy_template('report/style.css', out_dir)
    return index
