"""
Show versions of project(s) and corresponding packages

Usage: cznicinfo versions [-h] [-R] <project>...

Options:
    -R, --open-repology    Open repology.org versions page(s) in browser.

"""

from docopt import docopt

from cznicinfo import infocore
from cznicinfo import infoquery
from cznicinfo.log import getLogger
from cznicinfo import nv


log = getLogger(__name__)


def run_command(*args, **kwargs):
    cargs = docopt(__doc__)

    info = infocore.get_info()

    projs, not_found = infoquery.get_projects_by_name(info, cargs['<project>'])

    if not_found:
        nf_str = ", ".join(not_found)
        log.warn("Following projects were not found: %s" % nf_str)

    if not projs:
        log.warn("No projects selected.")
        return 1

    first = True
    for proj in projs:
        nv.save_config(proj)
        nv.run_check(proj)
        ds_vers = nv.load_versions(proj, 'downstream')
        us_vers = nv.load_versions(proj, 'upstream')
        if first:
            first = False
        else:
            print("")
        print("# %s downstream" % proj['full-name'])
        for repo, ver in ds_vers.items():
            print("%s: %s" % (repo, nv.pretty_version(ver)))
        if us_vers:
            print("\n# %s upstream" % proj['full-name'])
            for repo, ver in us_vers.items():
                print("%s: %s" % (repo, nv.pretty_version(ver)))

    return 0
