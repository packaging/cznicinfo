"""
List upstream projects

Usage: cznicinfo list-upstream [-h] [--order-by <field>] [--sort <order>]
                               [--max <N>] [--format <fmt>]
                               [--filter-repology]

Options:
    -O, --order-by <field>  Return projects ordered by (gitlab API):
                            id, name, path, created_at, updated_at, last_activity_at
                            Default: created_at
    -S, --sort <order>      Return projects sorted in 'asc' or 'desc' order.
                            Default: desc
    --max <N>               List max N projects.
                            Default: no limit
    -R, --filter-repology   Filter projects by valid repology.org link. (SLOW)
                            You can use extra --format option:
                              * %(repology_url)s - URL to repology versions page
    --format <fmt>          Python format string to print project with.
                            Default: '%(path_with_namespace)s: %(name)s'
""" # noqa

import json
import requests
from docopt import docopt

from cznicinfo import exception
from cznicinfo.log import getLogger
from cznicinfo import repology


log = getLogger(__name__)


GITLAB_INSTANCE = "https://gitlab.nic.cz"
CHOICE_PARAMS = {
    'order_by': [
        'id', 'name', 'path', 'created_at', 'updated_at', 'last_activity_at'
    ],
    'sort': ['asc', 'desc'],
}


def get_gitlab_projects(*args, **kwargs):
    projects = []
    page = 1
    total_pages = 1
    # parse arguments
    per_page = kwargs.get('per_page', 100)
    max_n = kwargs.get('max', None)
    if max_n:
        try:
            max_n = int(max_n)
        except ValueError:
            raise exception.InvalidType(
                var='max', desired='int', type=str(type(max_n).__name__))
        if per_page > max_n:
            per_page = max_n
    # prepare parameters to pass with request
    params = {
        'per_page': per_page
    }
    for var in CHOICE_PARAMS:
        choices = CHOICE_PARAMS[var]
        val = kwargs.get(var)
        if val:
            if val not in choices:
                raise exception.InvalidChoice(
                    var=var, val=val, opts=", ".join(choices))
            params[var] = val
    proj_url = "%s/api/v4/projects" % GITLAB_INSTANCE

    log.info("Getting gitlab projects from %s..." % GITLAB_INSTANCE)
    while page <= total_pages:
        params['page'] = page
        r = requests.get(proj_url, params=params)
        if not r.ok:
            raise exception.HTTPRequestFailed(
                request="GET %s" % proj_url, code=r.status_code)
        try:
            tp = int(r.headers.get('X-Total-Pages', '0'))
        except ValueError:
            tp = 0
        if tp > 0:
            total_pages = tp
        data = json.loads(r.content)
        projects += data
        if max_n and len(projects) >= max_n:
            projects = projects[:max_n]
            break
        log.info("   page %d of %d..." % (page, total_pages))
        page += 1
    return projects


def print_project(project, fmt=None):
    if not fmt:
        fmt = '%(path_with_namespace)s: %(name)s'

    try:
        print(fmt % project)
    except KeyError:
        raise exception.InvalidFormat(fmt=fmt)


def run_command(*args, **kwargs):
    cargs = docopt(__doc__)

    projects = get_gitlab_projects(
        order_by=cargs['--order-by'],
        sort=cargs['--sort'],
        max=cargs['--max'],
    )
    filter_repology = cargs['--filter-repology']

    for project in projects:
        if filter_repology:
            if not repology.repology_project_exists(project['path']):
                continue
            project['repology_url'] = repology.get_repology_versions_url(
                project['path'])
        print_project(project, fmt=cargs['--format'])

    return 0
