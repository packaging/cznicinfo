"""
Find projects using regexp

Usage: cznicinfo find [-h] [-f pretty|yaml|list] <query>...

Arguments:
    <query>  can be either:
             * python regex to match again relevant project attributes
             * ATTR:REGEX string where ATTR is a project attribute
               to match REGEX against. Lists kind of supported.

Options:
    -f, --format=<fmt>    Output format:
                          'pretty', 'yaml' or 'list' [default: pretty]
"""

from docopt import docopt

from cznicinfo import infocore
from cznicinfo import infoquery
from cznicinfo import infoprint


def run_command(*args, **kwargs):
    cargs = docopt(__doc__)
    fmt = cargs['--format']

    info = infocore.get_info()
    projs = infoquery.filter_projects(info, cargs['<query>'])

    if fmt == 'list':
        infoprint.pretty_list_projects(projs)
    elif fmt == 'yaml':
        infoprint.pretty_print_data(projs)
    else:
        # default --format is pretty print
        first = True
        for proj in projs:
            if first:
                first = False
            else:
                print()
            infoprint.pretty_print_project(proj)

    return 0
