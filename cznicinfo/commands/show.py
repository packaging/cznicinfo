"""
Show information about selected project(s)

Usage: cznicinfo show [-h] [-f pretty|yaml] <project>...

Options:
    -f, --format=<fmt>    Output format: 'pretty' or 'yaml' [default: pretty].
"""

from docopt import docopt

from cznicinfo import infocore
from cznicinfo import infoprint
from cznicinfo import infoquery
from cznicinfo.log import getLogger


log = getLogger(__name__)


def run_command(*args, **kwargs):
    cargs = docopt(__doc__)
    fmt = cargs['--format']

    info = infocore.get_info()
    projs, not_found = infoquery.get_projects_by_name(info, cargs['<project>'])

    if not_found:
        nf_str = ", ".join(not_found)
        log.error("Following projects were not found: %s" % nf_str)
        return 4

    if fmt == 'yaml':
        infoprint.pretty_print_data(projs)
    else:
        # default --format is pretty print
        first = True
        for proj in projs:
            if first:
                first = False
            else:
                print()
            infoprint.pretty_print_project(proj)

    return 0
