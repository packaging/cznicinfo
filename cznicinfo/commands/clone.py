"""
Clone project repo(s)

Usage: cznicinfo clone [-h] [-t <target>] [--dry] <project>...

Options:
    -t, --target=<target>  Target repo to clone:  (default: upstream)
                           * upstream: upstream source
                           * debian: debian package source (Salsa)
    --dry                  Dry run - only print clone command.
"""

from docopt import docopt

from cznicinfo import infocore
from cznicinfo import infoquery
from cznicinfo.log import getLogger, T
from cznicinfo.util.git import git


log = getLogger(__name__)


def run_command(*args, **kwargs):
    cargs = docopt(__doc__)
    target = cargs['--target'] or 'upstream'
    dry = cargs['--dry']

    if dry:
        log.info("{t.bold}DRY RUN{t.normal}: only printing commands "
                 "that would be run".format(t=T))

    info = infocore.get_info()
    projs, not_found = infoquery.get_projects_by_name(info, cargs['<project>'])

    if not_found:
        nf_str = ", ".join(not_found)
        log.error("Following projects were not found: %s" % nf_str)
        return 4

    for proj in projs:
        repo_url = infoquery.get_clone_target(proj, target=target)
        if dry:
            print("git clone %s" % repo_url)
        else:
            git('clone', repo_url, direct=True)

    return 0
