import yaml

from cznicinfo.log import getLogger, T


log = getLogger(__name__)


def pretty_list_projects(projects_info):
    for proj in projects_info:
        print("%s: %s" % (proj['short-name'], proj['full-name']))


def pretty_print_data(d):
    print(yaml.dump(d))


def pretty_print_project(project_info, indent=2):
    p = project_info.copy()
    space = indent * ' '
    # names first
    print("full-name: {t.white_bold}{v}{t.normal}".format(
        v=p['full-name'], t=T))
    print("short-name: {t.white_bold}{v}{t.normal}".format(
        v=p['short-name'], t=T))
    # upstream
    upstream = p.get('upstream')
    if upstream and 'url' in upstream:
        print("upstream: {t.cyan}{v}{t.normal}".format(
            v=upstream['url'], t=T))
    # links
    links = p.get('links')
    if links:
        print('links:')
        for link in links:
            print("{spc}- {title}: {t.cyan}{url}{t.normal}".format(
                spc=space, title=link['title'], url=link['url'], t=T))
    # mirrors
    mirrors = p.get('mirrors')
    if mirrors:
        print("mirrors:")
        for mr in mirrors:
            if 'url' in mr:
                print("{spc}- {t.cyan}{v}{t.normal}".format(
                    v=mr['url'], spc=space, t=T))
    # distro-pkgs
    distro_pkgs = p.get('distro-pkgs')
    if distro_pkgs:
        print("distro-pkgs:")
        for d in distro_pkgs:
            if 'distro' in d:
                print("{spc}- {t.green}{v}{t.normal}".format(
                    v=d['distro'], spc=space, t=T))
    # custom-pkgs
    custom_pkgs = p.get('custom-pkgs')
    if custom_pkgs:
        print("custom-pkgs:")
        for cp in custom_pkgs:
            if 'name' in cp and 'build-status-url' in cp:
                print("{spc}- {n}: {t.cyan}{v}{t.normal}".format(
                    n=cp['name'],
                    v=cp['build-status-url'],
                    spc=space, t=T))
