from pathlib import Path


cache_base_path = Path.home() / '.cache' / 'cznicinfo'
nv_cache_path = cache_base_path / 'nv'
nv_config_cache_path = nv_cache_path / 'config'
nv_versions_cache_path = nv_cache_path / 'versions'
report_cache_path = cache_base_path / 'report'


def ensure_cache_dirs():
    cache_base_path.mkdir(exist_ok=True)
    nv_cache_path.mkdir(exist_ok=True)
    nv_config_cache_path.mkdir(exist_ok=True)
    nv_versions_cache_path.mkdir(exist_ok=True)
    for stream in ['downstream', 'upstream']:
        (nv_config_cache_path / stream).mkdir(exist_ok=True)
        (nv_versions_cache_path / stream).mkdir(exist_ok=True)


def get_nv_config(name, stream='downstream'):
    return nv_config_cache_path / stream / ("%s.toml" % name)


def get_nv_versions(name, stream='downstream', postfix='new'):
    return nv_versions_cache_path / stream / ("%s_%s.json" % (name, postfix))
