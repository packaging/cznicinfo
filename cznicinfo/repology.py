import requests


REPOLOGY_VERSIONS_URL = "https://repology.org/project/%(name)s/versions"
REPOLOGY_INFORMATION_URL = "https://repology.org/project/%(name)s/information"


def get_repology_versions_url(project):
    return REPOLOGY_VERSIONS_URL % {'name': project}


def repology_project_exists(project):
    url = REPOLOGY_INFORMATION_URL % {'name': project}
    r = requests.get(url)
    return r.ok
