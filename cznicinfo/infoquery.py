import re
from collections.abc import Iterable
from functools import partial

from cznicinfo import exception


def get_projects_by_name(info, names):
    found_projs = []
    not_found = []

    for pname in names:
        found = False
        for proj in info['projects']:
            if (proj['short-name'] == pname or
               proj['full-name'] == pname):
                found_projs.append(proj)
                found = True
                break
        if not found:
            not_found.append(pname)

    return found_projs, not_found


def split_query(query):
    attr, _, rex = query.partition(':')
    if rex:
        return attr, rex
    return None, attr


def filter_projects(info, queries):
    projs = info.get('projects', [])
    qs = list(map(split_query, queries))
    filter_wrapper = partial(_match_project, qs)
    filtered_projs = list(filter(filter_wrapper, projs))
    return filtered_projs


def match_project_by_attr_rex(project, attr, rex):
    val = project.get(attr)
    if val is None:
        return False
    if isinstance(val, str):
        if not re.search(rex, val):
            return False
    elif isinstance(val, Iterable):
        # collection matches if any item of collection matches
        found = False
        for e in val:
            if re.search(rex, e):
                found = True
                break
        if not found:
            return False
    else:
        raise exception.InvalidProjectFilter(
            why=("Can only filter strings but '%s' is %s"
                 % (attr, type(rex).__name__)))
    return True


def match_project_by_smart_rex(project, rex):
    if re.search(rex, project['short-name']):
        return True
    if re.search(rex, project['full-name']):
        return True
    # TODO: match more stuff like URLs
    return None


def _match_project(queries, project):
    for attr, rex in queries:
        if attr:
            m = match_project_by_attr_rex(project, attr, rex)
        else:
            m = match_project_by_smart_rex(project, rex)
        if not m:
            return False
    return True


CLONE_TARGETS = ['upstream', 'debian']
DEBIAN_SALSA_GIT_HTTPS = "https://salsa.debian.org/%s.git/"


def get_clone_target(project, target='upstream'):
    pname = project.get('short-name')
    if target == 'upstream':
        upstream = project.get('upstream')
        if not upstream:
            raise exception.InsufficientProjectInfo(
                proj=pname, item="upstream")
        repo_url = upstream.get('url')
        if not repo_url:
            raise exception.InsufficientProjectInfo(
                proj=pname, item="upstream.url")
        return repo_url
    elif target == 'debian':
        distro_pkgs = project.get('distro-pkgs')
        if not distro_pkgs:
            raise exception.InsufficientProjectInfo(
                proj=pname, item="distro-pkgs")
        path = None
        for distro in distro_pkgs:
            if distro.get('distro') == target:
                path = distro.get('path')
                if not path:
                    raise exception.InsufficientProjectInfo(
                        proj=pname,
                        item="distro-pkgs[distro=%s].path" % target)
                break
        if not path:
            raise exception.InsufficientProjectInfo(
                proj=project,
                item="distro-pkgs[distro=%s]" % target)
        repo_url = DEBIAN_SALSA_GIT_HTTPS % path
        return repo_url
    else:
        raise exception.InvalidChoice(
            var='target', val=target, opts=CLONE_TARGETS)
