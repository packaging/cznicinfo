import jinja2
from pathlib import Path
import shutil


def get_template_path(template_fn, module_path=None):
    if module_path:
        path = module_path
    else:
        import cznicinfo
        path = Path(cznicinfo.__file__).parent / 'templates'

    return path / template_fn


def render_template(template_path, out_dir, vars):
    src = get_template_path(template_path)
    dst = out_dir / src.name
    with src.open('r') as srcf:
        t = jinja2.Template(srcf.read())
    with dst.open('w') as dstf:
        dstf.write(t.render(**vars) + '\n')
    return dst


def copy_template(template_path, out_dir):
    src = get_template_path(template_path)
    dst = out_dir / src.name
    shutil.copyfile(src, dst)
    return dst
