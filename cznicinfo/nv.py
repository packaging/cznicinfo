from collections import defaultdict
import json
from packaging import version
import re

from cznicinfo import cache
from cznicinfo import infocore
from cznicinfo.log import getLogger
from cznicinfo.util.run import run


log = getLogger(__name__)


VERSION_STATES = ['current-ver', 'old-ver', 'no-ver', 'all-ver']
VERSION_STREAMS = ['downstream', 'upstream']


def save_config(project):
    save_downstream_config(project)
    save_upstream_config(project)


def save_downstream_config(project):
    name = project['short-name']
    distros = infocore.get_supported_distros()
    cache.ensure_cache_dirs()
    out_path = cache.get_nv_config(name)
    outf = open(out_path, 'w')
    log.info("saving downstream nvchecker config: %s", out_path)
    dpkgs = project.get('distro-pkgs', [])
    outf.write(get_nv_head(name) + "\n")
    for dpkg in dpkgs:
        distro_name = dpkg.get('distro')
        if distro_name not in distros:
            msg = "current distro info not available for: %s"
            log.warn(msg, distro_name)
            continue
        distro = distros[distro_name]
        log.verbose("generating entries for distro: %s", distro_name)
        for release in distro.get('releases', []):
            entry = get_nv_entry(project, distro, release)
            outf.write(entry + "\n")


def save_upstream_config(project):
    name = project['short-name']
    nvconfig = project.get('upstream', {}).get('nvconfig')
    if not nvconfig:
        return False
    template_path = infocore.get_upstream_nv_template_path(nvconfig)
    config_path = cache.get_nv_config(name, 'upstream')
    distros = infocore.get_supported_distros()
    content = open(template_path).read()
    log.info("saving upstream nvchecker config: %s", config_path)
    with open(config_path, 'w') as outf:
        outf.write(get_nv_head(name, 'upstream') + "\n")
        outf.write(content)


def run_check(project):
    for stream in VERSION_STREAMS:
        cfg = cache.get_nv_config(project['short-name'], stream)
        if not cfg.exists():
            continue
        log.info("running nvchecker on %s config: %s", stream, cfg)
        run('nvchecker', '-c', cfg)


def run_upstream_check(project):
    cfg = cache.get_nv_config(project['short-name'], 'upstream')
    pass


def load_versions(project, stream='downstream', postfix='new'):
    verfile = cache.get_nv_versions(
        project['short-name'], stream, postfix)
    try:
        return json.load(verfile.open())
    except FileNotFoundError:
        return {}


def versions_by_distro(project):
    distros = infocore.get_supported_distros()
    nvers = {}
    for stream in VERSION_STREAMS:
        nvers[stream] = load_versions(project, stream)

    dpkgs = project.get('distro-pkgs', [])

    # determine highest version
    last_ver = version.parse("0")
    for dpkg in dpkgs:
        dpkg_distro = dpkg.get('distro')
        if dpkg_distro not in distros:
            continue
        distro = distros[dpkg_distro]
        distro_name = distro['name']
        for release in distro.get('releases', []):
            distro_version = release['version']
            distro_id = nvid(distro_name, distro_version)
            for stream in VERSION_STREAMS:
                v = pretty_version(nvers[stream].get(distro_id))
                v = version.parse(v or '0')
                last_ver = max(v, last_ver)

    # generate report data
    versions = []
    for dpkg in dpkgs:
        dpkg_distro = dpkg.get('distro')
        if dpkg_distro not in distros:
            msg = "distro info not available for: %s"
            log.warn(msg, dpkg_distro)
            continue
        distro = distros[dpkg_distro]
        distro_name = distro['name']

        distro_vers = []
        for release in distro.get('releases', []):
            distro_version = release['version']
            distro_id = nvid(distro_name, distro_version)
            tags = release.get('tags', [])
            dvers = {}
            for stream in VERSION_STREAMS:
                ver = nvers[stream].get(distro_id)
                if not ver:
                    continue
                ver = pretty_version(ver)
                if version.parse(ver or '0') >= last_ver:
                    stag = 'current-ver'
                elif ver:
                    stag = 'old-ver'
                stags = [stag] + tags
                dvers[stream] = {
                    'version': ver,
                    'tags': stags,
                }
            distro_vers.append({
                'distro_version': distro_version,
                'versions': dvers,
            })

        versions.append({
            'distro': distro_name,
            'versions': distro_vers,
            })

    return versions


def versions_by_distro2tables(versions):
    no_version = {
        'version': None,
        'tags': ['no-ver'],
    }
    distro_tables = []
    for distro in versions:
        distro_name = distro['distro']
        header = [distro_name]
        vers = {}
        # check present version streams (each has one row)
        present_streams = set()
        for dv in distro['versions']:
            present_streams.update(dv['versions'])
        streams = []
        for stream in VERSION_STREAMS:
            if stream in present_streams:
                streams.append(stream)
        for stream in streams:
            # row starts with version type (upstream, downstream, ...)
            vers[stream] = [{'version': stream}]
        for dv in distro['versions']:
            header.append(dv['distro_version'])
            for stream in streams:
                vers[stream].append(dv['versions'].get(stream, no_version))
        # transform vers to flat rows
        rows = []
        for stream in streams:
            rows.append(vers[stream])
        # append to result
        distro_tables.append({
            'header': header,
            'rows': rows,
        })
    return distro_tables


def count_versions_tags(versions):
    # excepts result of versions_by_distro() as input
    counts = {'total': get_vstates_dict()}
    for distro in versions:
        for dv in distro['versions']:
            for vertype, ver in dv['versions'].items():
                if vertype not in counts:
                    counts[vertype] = get_vstates_dict()
                tags = ['all-tags'] + ver['tags']
                for verstate in VERSION_STATES:
                    if verstate in tags or verstate == 'all-ver':
                        for tag in tags:
                            if tag in VERSION_STATES:
                                continue
                            counts[vertype][verstate][tag] += 1
                            counts['total'][verstate][tag] += 1
    return counts


def get_vstates_dict():
    d = {}
    for st in VERSION_STATES + ['all-ver']:
        d[st] = defaultdict(int)
    return d


def get_nv_entry(project, distro, release):
    pkg_name = project['short-name']
    distro_name = distro['name']
    distro_version = str(release['version'])
    nv_source = distro['nv_source']
    distro_id = nvid(distro_name, distro_version)
    e = "[%s]\n" % distro_id
    e += 'source = "%s"\n' % nv_source
    e += '%s = "%s"\n' % (nv_source, pkg_name)
    for arg, val in release.get('nv', {}).items():
        e += '%s = "%s"\n' % (arg, val)
    return e


def get_nv_head(name, stream='downstream'):
    old_path = cache.get_nv_versions(name, stream, 'old')
    new_path = cache.get_nv_versions(name, stream, 'new')
    return ('[__config__]\n'
            'oldver = "{old}"\n'
            'newver = "{new}"\n'.format(
                old=old_path, new=new_path))


def nvid(distro, version):
    dv = "%s-%s" % (distro, version)
    return re.sub(r'\W+', '-', dv.lower())


def pretty_version(version):
    if not version:
        return version
    ver, _, _ = version.partition('-')
    return ver
