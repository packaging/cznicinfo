import yaml
import os
from collections import defaultdict

from cznicinfo.log import getLogger
from cznicinfo import exception


log = getLogger(__name__)


DEFAULT_RELEASE = {
    'release': 'master',
    'branch': 'master'
}


def get_info_path(module_path=None):
    return get_data_path('cznicinfo.yaml', module_path=module_path)


def get_supported_distros_path(module_path=None):
    return get_data_path('supported_distros.yaml', module_path=module_path)


def get_upstream_nv_template_path(name, module_path=None):
    fn = 'upstream-nv/%s' % name
    return get_data_path(fn, module_path=module_path)


def get_data_path(data_fn, module_path=None):
    if module_path:
        path = module_path
    else:
        import cznicinfo
        path = os.path.dirname(cznicinfo.__file__)
    return os.path.join(path, 'data', data_fn)


def get_info(data_path=None):
    if not data_path:
        data_path = get_info_path()
    info = load_info(data_path)
    return parse_info(info)


def get_supported_distros(data_path=None):
    if not data_path:
        data_path = get_supported_distros_path()
    return load_info(data_path)


def parse_info(info):
    info = parse_projects(info)
    return info


def load_info(info_path=None):
    log.debug("Loading info file: %s", info_path)
    with open(info_path, 'r') as stream:
        data = yaml.safe_load(stream)
    return data


def parse_projects(info):
    if 'projects' not in info:
        raise exception.MissingRequiredInfoItem(item='projects list')

    unique_items = ['short-name', 'full-name']
    unique_lists = defaultdict(set)

    for i, proj in enumerate(info['projects']):
        for item_name in unique_items:
            item_value = proj.get(item_name)
            if not item_value:
                raise exception.MissingRequiredInfoItem(
                        item='projects[%d].%s' % (i, item_name))
            if item_value in unique_lists[item_name]:
                item_str = 'projects[%d].%s = %s' % (i, item_name, item_value)
                raise exception.DuplicateInfoProjects(item=item_str)
            unique_lists[item_name].add(item_value)

        if 'releases' not in proj:
            proj['releases'] = [DEFAULT_RELEASE]
    return info
