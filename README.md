# cznicinfo

is a python module and CLI tool for querying project and packaging information
for CZ.NIC open source projects.

The goal of `cznicinfo` is to collect packaging metadata from various repos
and minds of packaging wizards alike in order to provide unified packaging
metadata required for high quality packaging across distros. Providing such
data in easily parsable format benefits future packagers but more importantly
it allows full packaging automation.

`cznicinfo` also acts as [nvchecker](https://github.com/lilydjwg/nvchecker)
fronend for checking package versions in various downstream and upstream repos.

## status

You can see `cznicinfo report` updated daily on
[pkg.labs.nic.cz][report].

`cznicinfo` is currently in early alpha.

Please see the the YAML file containing the information:
[data/cznicinfo.yaml](https://gitlab.nic.cz/packaging/cznicinfo/-/blob/master/cznicinfo/data/cznicinfo.yaml)

## installation

`cznicinfo` requires Python >= 3.7.

`cznicinfo` uses [pbr](https://docs.openstack.org/pbr) to handle python
packaging. It's natively packaged on most distros as `python-pbr` or
`python3-pbr`. Examples of installing `pbr` on common distros:

    # debian/ubuntu
    apt install python3-pbr

    # fedora/EPEL
    dnf install python3-pbr

    # arch
    pacman -S python-pbr

It should pull in `python-setuptools` as well.

Other required python modules are listed in
[requirements.txt](requirements.txt) and are handled automatically by `pbr` as
required when using `setup.py`.

With `pbr` installed, you can use `setup.py` as you would with any python
package:

    # user development install
    python3 setup.py develop --user

    # install (inside venv)
    python3 setup.py install

    # get source tarball
    python3 setup.py sdist

    # etc., see python setup.py --help-commands

Note that you can manually install requirements using `pip3 install -r requirements.txt`.

## usage

Run `cznicinfo` without parameters to get possible commands:

    CZ.NIC info

    Usage: cznicinfo <command> [<args>...]
        cznicinfo <command> --help
        cznicinfo [--help | --version]

    Commands:
    list            List available projects
    find            Find projects using regexp
    show            Show project(s) information
    versions        Show versions of a project and its packages
    report          Generate report of selected projects' versions
    clone           Clone project repo(s)
    list-upstream   List upstream git repos

    Options:
    -h --help     Show help screen, can be used after a command
    --version     Show version

Example commands:

    cznicinfo find knot

    cznicinfo versions knot-resolver

    cznicinfo report

Docs Pending™

## sneak peak

Here, have a picture of CLI usage:

![cznicinfo CLI sneak peak](sneak_peak.png "cznicinfo CLI sneak peak image")

See [cznicinfo report][report].

[report]: https://pkg.labs.nic.cz
